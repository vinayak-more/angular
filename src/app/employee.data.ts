import { Employee } from './model/employee';

export const data: Employee[] = [
    {
        id:1,
        name: "Jack",
        designation: "Engineer",
        salary: 10000
    },
    {
        id:2,
        name: "Jill",
        designation: "Manager",
        salary: 20000
    },
    {
        id:3,
        name: "Sam",
        designation: "Clerk",
        salary: 30000
    },
    {
        id:4,
        name: "Neil",
        designation: "Tester",
        salary: 40000
    }
];
import { Injectable } from "@angular/core";
import { data } from '../employee.data';
import { Employee } from '../model/employee';
import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/of";

@Injectable()
export class EmployeeService {

    private employees: Employee[] = data;

    private employee: Employee;


    getEmployee() {
        return this.employee;
    }

    setEmployee(employee: Employee) {
        this.employee = employee;
    }

    constructor() {

    }

    public get(): Observable<Employee[]> {
        return Observable.of(this.employees);
    }

    public saveOrUpdate(employee: Employee) {
        if (employee.id > 0) {
            this.update(employee);
        } else {
            this.save(employee);
        }
    }

    public save(employee: Employee) {
        employee.id = this.employees.length + 1;
        this.employees.push(employee);
    }

    public update(employee: Employee) {
        let employeeOld = this.employees.find(e => e.id === employee.id);
        let indexOfEmployee = this.employees.indexOf(employeeOld);
        this.employees[indexOfEmployee] = employee;

    }

    public delete(employee: Employee) {
        let employeeOld = this.employees.find(e => e.id === employee.id);
        let indexOfEmployee = this.employees.indexOf(employeeOld);
        this.employees.splice(indexOfEmployee, 1);
    }

}
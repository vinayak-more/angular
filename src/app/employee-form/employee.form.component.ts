import { Component, OnInit } from '@angular/core';
import { Employee } from '../model/employee';
import { EmployeeService } from '../service/employee.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit {

  employee: Employee
  constructor(private employeeService: EmployeeService, private router: Router) { }

  ngOnInit() {
    this.employee = this.employeeService.getEmployee() == null ? new Employee() : this.employeeService.getEmployee();
  }

  save() {
    this.router.navigate(['']);
    this.employeeService.saveOrUpdate(this.employee);
  }

  cancel() {
    this.router.navigate(['']);
    this.employeeService.setEmployee(null);
  }

}

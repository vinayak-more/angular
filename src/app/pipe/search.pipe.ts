import { Pipe, PipeTransform } from '@angular/core';
import { Employee } from '../model/employee';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(items: Employee[], value: string): Employee[] {
    if (!items) {
      return [];
    }
    if (!value) {
      return items;
    }
    value=value.toLowerCase();
    return items.filter(item => item.name.toLowerCase().includes(value)
      || item.designation.toLowerCase().includes(value)
      || item.salary.toString().includes(value));
  }

}

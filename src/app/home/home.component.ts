import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../service/employee.service';
import { Employee } from '../model/employee';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  employees:Employee[];
  searchString:string;

  constructor(private employeeService:EmployeeService,private router:Router) {}

  ngOnInit() {
    this.employeeService.get().subscribe(e=>this.employees=e);
  }
  deleteEmployee(employee:Employee){
    this.employeeService.delete(employee);
  }

  editEmployee(employee:Employee){
    this.employeeService.setEmployee(employee);
    this.router.navigate(['employee']);
  }

  addEmployee(){
    this.employeeService.setEmployee(new Employee());
    this.router.navigate(['employee']);
  }

  

}
